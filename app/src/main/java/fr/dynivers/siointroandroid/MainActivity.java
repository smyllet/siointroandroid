package fr.dynivers.siointroandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    RadioGroup radioGroup;
    RadioButton radioButton;
    Button button;

    static final String EXTRA_MESSAGE = "passe a ton voisin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.sio_texte);
        button = findViewById(R.id.button);
        radioGroup = findViewById(R.id.matiereSelectGroup);

        button.setEnabled(false);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                button.setEnabled(true);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButton = findViewById(radioGroup.getCheckedRadioButtonId());

                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra(MainActivity.EXTRA_MESSAGE, radioButton.getText().toString());
                startActivity(intent);
            }
        });
    }
}