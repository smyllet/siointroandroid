package fr.dynivers.siointroandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.widget.TextView;

public class MessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Intent intent = getIntent();

        String messageRecu = intent.getStringExtra(MainActivity.EXTRA_MESSAGE).toLowerCase();

        TextView monDiffuseur = findViewById(R.id.diffuseMessage);

        if(messageRecu.equals("slam 3")) monDiffuseur.setText(Html.fromHtml(getString(R.string.text_slam3)));
        else if(messageRecu.equals("slam 4")) monDiffuseur.setText(getString(R.string.text_slam4));
        else
        {
            monDiffuseur.setText(messageRecu);
        }
    }
}